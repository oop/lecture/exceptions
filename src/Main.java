import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main {

	static class NominatorException extends RuntimeException {
		public NominatorException(String message) {
			super(message);
		}
	}

	static class DeNominatorException extends RuntimeException {
		public DeNominatorException(String message) {
			super(message);
		}
	}

	public static SpaceShip voyager;

	private static SpaceShip[] vulcanShips = new SpaceShip[10];

	static void readFile() throws IOException {

		File textFile = new File("input.txt");
		FileReader reader = new FileReader(textFile);
		BufferedReader bufferedReader = new BufferedReader(reader);

		String line = bufferedReader.readLine();

	}

	static double divide(double a, double b) throws Main.DeNominatorException, Main.NominatorException {

		if (b == 0) {
			throw new DeNominatorException("Please don't use value 0 for b");
		}

		if (a == 0) {
			throw new NominatorException("Result will be 0");
		}
		return a / b;
	}

	public static void main(String[] args) {
		System.out.println("These are Exceptions");

		double res = -1.0;

		try {
			res = divide(0, 0);
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.printf("result: %.2f\n", res);
		System.out.println("End of Program");

	}
}
